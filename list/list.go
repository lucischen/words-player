package list

import (
	"fmt"
	"math/rand"
	"os"
	"time"
)

type List struct {
	Title     string      `json:"-" yaml:"-"`
	StartAt   int64       `json:"start_at,omitempty" yaml:"start_at"`
	WordsList []WordsList `json:"words" yaml:"words_list"`
}

type WordsList struct {
	Word     string `json:"word" yaml:"word"`
	Page     int    `json:"page" yaml:"page"`
	Level    int    `json:"level" yaml:"level"`
	Sentence string `json:"sentence" yaml:"sentence"`
}

type Parser interface {
	Unmarshal([]byte, interface{}) error
}

func Parse(raw []byte, parser Parser) List {
	l := List{}
	err := parser.Unmarshal(raw, &l)
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}

	return l
}

func Shuffle(wordsList []WordsList) []WordsList {
	rand.Seed(time.Now().UnixNano())
	rand.Shuffle(len(wordsList), func(i, j int) {
		wordsList[i], wordsList[j] = wordsList[j], wordsList[i]
	})

	return wordsList
}
