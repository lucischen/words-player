package parser

import (
	"encoding/json"

	yaml "gopkg.in/yaml.v2"
)

var (
	YAML = &yamlParser{}
	JSON = &jsonParser{}
)

type yamlParser struct{}

func (y yamlParser) Marshal(obj interface{}) ([]byte, error) {
	return yaml.Marshal(obj)
}

func (y yamlParser) Unmarshal(raw []byte, obj interface{}) error {
	return yaml.Unmarshal(raw, obj)
}

type jsonParser struct{}

func (j jsonParser) Marshal(obj interface{}) ([]byte, error) {
	return json.Marshal(obj)
}
func (j jsonParser) Unmarshal(raw []byte, obj interface{}) error {
	return json.Unmarshal(raw, obj)
}
