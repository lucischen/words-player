package config

import (
	"log"

	yaml "gopkg.in/yaml.v2"
)

type conf struct {
	BookList []string `yaml:"book_list"`
}

func Parse(raw []byte) *conf {
	c := &conf{}
	err := yaml.Unmarshal(raw, c)
	if err != nil {
		log.Fatalf("Unmarshal: %v", err)
	}
	return c
}
