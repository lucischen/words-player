package loader

import (
	"errors"
	"fmt"
	"io/ioutil"
	"os"
)

var (
	ErrOnWriteFile = errors.New("Write File Error")
)

type Loader struct{}

func New() *Loader {
	return &Loader{}
}

func (l *Loader) Read(filename string) []byte {
	raw, err := ioutil.ReadFile(filename)
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}

	return raw
}

func (l *Loader) Write(path string, raw []byte) {
	err := ioutil.WriteFile(path, raw, 0644)
	if err != nil {
		fmt.Println(ErrOnWriteFile.Error(), err.Error(), fmt.Sprintf("path: %v", path))
	}
}
