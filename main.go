package main

import (
	"flag"
	"fmt"
	"lucischen/words-player/config"
	"lucischen/words-player/list"
	"lucischen/words-player/loader"
	"lucischen/words-player/parser"
	"time"
)

var marked = []list.WordsList{}

const (
	configPath = "./books/list.yaml"
	// output marked words
	markedFilePath = "./books/marked.yaml"
)

var (
	// flag
	readPath string
)

func init() {
	// 輸入要讀檔案的路徑或者使用預設
	flag.StringVar(&readPath, "read", markedFilePath, "file path, read file")
	flag.Parse()
}

func main() {
	lo := loader.New()

	// 如果有值就表示讀取外部資源
	if readPath != markedFilePath {
		// 讀取單字本
		raw := lo.Read(readPath)
		run(raw)
		return
	}

	// 讀取單字清單
	raw := lo.Read(configPath)
	books := config.Parse(raw)

	// 等待使用者輸入書目
	title := ""
	fmt.Println("Please enter a title: ")
	fmt.Scanln(&title)

	// 檢查 boods 內有沒有該書
	var hasBook = false
	for _, book := range books.BookList {
		if book == title {
			hasBook = true
		}
	}
	if !hasBook {
		fmt.Printf("No books named %v", title)
		return
	}

	// 讀取單字本
	raw = lo.Read("./books/" + title + ".yaml")
	run(raw)
}

func run(raw []byte) {
	parsedList := list.Parse(raw, parser.YAML)
	shuffledList := list.Shuffle(parsedList.WordsList)

	//排版用分隔線
	fmt.Println("----------------------------------------")

	listLen := len(shuffledList)
	// 認單字
	for i, el := range shuffledList {
		fmt.Printf("%v: %v (%v/%v)\n", el.Word, el.Sentence, i+1, listLen)

		fmt.Println("mark it?(m/mark, q/quiet)")
		command := ""
		fmt.Scanln(&command)

		if command == "m" || command == "mark" {
			marked = append(marked, el)
		}

		if command == "q" || command == "quiet" {
			break
		}
	}

	// 如果沒有被標註的單字就不用寫入檔案
	if len(marked) == 0 {
		return
	}

	// 紀錄被標注的單字
	// 轉型
	markedlist, _ := parser.YAML.Marshal(list.List{
		StartAt:   time.Now().Unix(),
		WordsList: marked,
	})
	// 寫入檔案
	lo := loader.New()
	lo.Write(markedFilePath, markedlist)
}
